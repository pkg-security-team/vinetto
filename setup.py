from setuptools import setup

setup(
  name='vinetto',
  version='0.8.0',
  scripts=['bin/vinetto'],
  include_package_data=True,
  packages=['vinetto'],
  description='Vinetto: The Thumbnail File Parser',
  author='Michel Roukine',
  author_email='rukin@users.sf.net',
  maintainer='Keven L. Ates',
  maintainer_email='atescomp@gmail.com',
  url='https://github.com/AtesComp/Vinetto',
  license='GNU GPLv3',
  platforms='LINUX',
)
